# swisscovery id extractor

Swisscovery search results contain a column bundling all identificators like ISSN, DOI etc.

This script reads and separates those into multiple columns. This way they can be used easily in spreadsheets for identification or for searches in other databases.

# Requirements

- Python >= 3.9
- pandas package (pip install pandas)

# Get started

- save your search results in excel format
- set input_file_path to your xls and adjust output_file_path if needed
- run script to create output file and check if all lines have been processed