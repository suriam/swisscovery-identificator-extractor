""" 
    Swisscovery search results contain a column bundling all identificators like ISSN, DOI etc.
    This script reads and separates those into multiple columns. This way they can be used easily 
    in spreadsheets for identification or for searches in other databases.
"""

import pandas as pd

# Path to swisscovery search result Excel file
input_file_path = 'input/results.xlsx'

output_file_path = 'ouput/results-ids-fixed.xlsx'

# Read the Excel file into a DataFrame
df = pd.read_excel(input_file_path)

# Create empty columns for 'DOI', 'PMID', 'OCLC', 'EISBN', and 'ISBN' if they don't exist
for column in ['DOI', 'PMID', 'OCLC', 'EISBN', 'ISBN']:
    if column not in df.columns:
        df[column] = ''

# Function to extract identifiers from 'Identifikator' column
def extract_identifiers(identifikator):
    doi = None
    pmid = None
    oclc = None
    eisbn = None
    isbn = None

    # Check if identifikator is not a string
    if isinstance(identifikator, str):
        # Check for DOI
        doi_start = identifikator.find('DOI: ')
        if doi_start != -1:
            doi_end = identifikator.find(';', doi_start)
            if doi_end == -1:
                doi = identifikator[doi_start + 5:]
            else:
                doi = identifikator[doi_start + 5:doi_end]
        
        # Check for PMID
        pmid_start = identifikator.find('PMID: ')
        if pmid_start != -1:
            pmid_end = identifikator.find(';', pmid_start)
            if pmid_end == -1:
                pmid = identifikator[pmid_start + 6:]
            else:
                pmid = identifikator[pmid_start + 6:pmid_end]

        # Check for OCLC
        oclc_start = identifikator.find('OCLC: ')
        if oclc_start != -1:
            oclc_end = identifikator.find(';', oclc_start)
            if oclc_end == -1:
                oclc = identifikator[oclc_start + 6:]
            else:
                oclc = identifikator[oclc_start + 6:oclc_end]

        # Check for EISBN
        eisbn_start = identifikator.find('EISBN: ')
        if eisbn_start != -1:
            eisbn_end = identifikator.find(';', eisbn_start)
            if eisbn_end == -1:
                eisbn = identifikator[eisbn_start + 7:]
            else:
                eisbn = identifikator[eisbn_start + 7:eisbn_end]

        # Check for ISBN
        isbn_start = identifikator.find('ISBN: ')
        if isbn_start != -1:
            isbn_end = identifikator.find(';', isbn_start)
            if isbn_end == -1:
                isbn = identifikator[isbn_start + 6:]
            else:
                isbn = identifikator[isbn_start + 6:isbn_end]

    return doi, pmid, oclc, eisbn, isbn

# Loop through rows and update columns
for index, row in df.iterrows():
    identifikator = row['Identifikator']
    doi, pmid, oclc, eisbn, isbn = extract_identifiers(identifikator)
    df.at[index, 'DOI'] = doi
    df.at[index, 'PMID'] = pmid
    df.at[index, 'OCLC'] = oclc
    df.at[index, 'EISBN'] = eisbn
    df.at[index, 'ISBN'] = isbn

# Save the updated DataFrame to a different output file
df.to_excel(output_file_path, index=False)

print("Identifiers extraction and update complete. Data saved to", output_file_path)
